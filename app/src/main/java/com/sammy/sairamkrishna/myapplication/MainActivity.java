package com.sammy.sairamkrishna.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.Manifest.permission.CAMERA;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {

    private Button btnGerarQR;
    private Button btnLerQR;
    TextView editSobre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editSobre = (TextView) findViewById(R.id.txtSobre);
        btnLerQR = (Button) findViewById(R.id.btnLerQR);
        btnGerarQR = (Button) findViewById(R.id.btnGerarQR);


        btnLerQR.setOnClickListener(this);
        btnGerarQR.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        editSobre.setText(Constant.email);

    }

    @Override
    public void onClick(View view) {

        Intent intent;
        switch (view.getId()) {
            case R.id.btnGerarQR:
                intent = new Intent(this, GeneratorActivity.class);
                startActivity(intent);
                break;
            case R.id.btnLerQR:
                intent = new Intent(this, ScanActivity.class);
                startActivity(intent);

                break;

        }
    }


}
